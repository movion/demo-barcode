package es.movion.demobarcode.controller;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import es.movion.demobarcode.MovionApp;
import es.movion.demobarcode.R;

public class MediaPlayerController {

    private static MediaPlayerController instance;
    private MediaPlayer mediaPlayer = null;
    private Context context;
    private int rawID = 0;

    private MediaPlayerController() {
    }

    public static MediaPlayerController getInstance() {
        if (instance == null)
            instance = new MediaPlayerController();
        return instance;
    }

    public void initPlayer(int resId) {
        this.context = MovionApp.getAppContext();
        this.rawID = resId;
    }

    public void playSuccess() {
        initPlayer(R.raw.tag_inventoried);
        play();
    }

    private void play() {
        if (context == null) {
            Log.e(MediaPlayerController.class.getCanonicalName(), "Error: se ha intentado reproducir sin el contexto de la actividad");
            return;
        }

        if (rawID == 0) {
            Log.e(MediaPlayerController.class.getCanonicalName(), "Error: no se ha encontrado un recurso válido para reproducir");
            return;
        }

        release();
        mediaPlayer = MediaPlayer.create(context, rawID);
        mediaPlayer.start();
    }

    public void release() {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

}
