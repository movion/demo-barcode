package es.movion.demobarcode;

import android.app.Application;
import android.content.Context;


public class MovionApp extends Application {

    private static final String LOG_TAG_APP_NAME = "MercaGesApp";
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        MovionApp.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return context;
    }

}
