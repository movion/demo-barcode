package es.movion.demobarcode.ui;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.Toast;

import com.senter.support.openapi.StBarcodeScanner;

import java.nio.charset.StandardCharsets;

import es.movion.demobarcode.R;
import es.movion.demobarcode.controller.MediaPlayerController;

public class MainActivity extends PermissionsActivity {

    private StBarcodeScanner scanner;
    private boolean isScanning;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Adquire the reader instance.
        scanner = StBarcodeScanner.getInstance();
        if (scanner == null) {
            Toast.makeText(this, R.string.error_scanner_null, Toast.LENGTH_SHORT).show();
            this.finish();
        }
        textView = findViewById(R.id.scan_text);
    }

    private boolean isScannerActive() {
        return scanner != null;
    }

    @Override
    protected void onDestroy() {
        if (!isScannerActive())
            scanner.uninit();
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (isScanKey(keyCode)) {
            scanItem();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    private boolean isScanKey(int keyCode) {
        return KeyEvent.KEYCODE_WAKEUP == keyCode ||
                KeyEvent.KEYCODE_F1 == keyCode ||
                KeyEvent.KEYCODE_F2 == keyCode
                || 422 == keyCode
                || 421 == keyCode;
    }

    /**
     * Scans an item. Only one scan thread can be running at a time.
     */
    private void scanItem() {
        if(checkCameraPermissions()) {
            if (isScanning)
                return;
            isScanning = true;
            new Thread() {
                @Override
                public void run() {
                    try {
                        StBarcodeScanner.BarcodeInfo rslt = scanner.scanBarcodeInfo();//scan ,if failed,null will be return
                        String result = null;
                        if (rslt != null)
                            result = new String(rslt.getBarcodeValueAsBytes(), StandardCharsets.UTF_8);
                        handleScanMessage(result);
                    } catch (InterruptedException e) {
                        showError();
                    } finally {
                        isScanning = false;
                    }
                }
            }.start();
        }
    }

    /**
     * Updates the UI in the main thread
     * @param message the scan result
     */
    private void handleScanMessage(final String message) {
        if(message != null)
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MediaPlayerController.getInstance().playSuccess();
                    textView.setText(message);
                }
            });
    }

    private void showError() {
        Toast.makeText(this, R.string.error_scan_interrupted, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPermissionsSuccess(int requestCode) {
        scanItem();
    }
}