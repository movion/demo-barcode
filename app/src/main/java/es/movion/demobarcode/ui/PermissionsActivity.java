package es.movion.demobarcode.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import es.movion.demobarcode.R;


public abstract class PermissionsActivity extends AppCompatActivity {
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    public static final int MY_PERMISSIONS_REQUEST_EXTERNAL = 2;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkCameraPermissions() {
        boolean grantedCamera = isGranted(Manifest.permission.CAMERA);
        if (!grantedCamera) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
        }
        return grantedCamera;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkExternalPermissions() {
        boolean grantedWrite = isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (!grantedWrite) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_EXTERNAL);
        }
        return grantedWrite;
    }


    public boolean isGranted(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == 0;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean permissionsGranted = isAllGranted(permissions);
        if (permissionsGranted) {
            onPermissionsSuccess(requestCode);
        } else {
            onPermissionsDenied(requestCode, permissions);
        }
    }

    private boolean isAllGranted(String[] permissions) {
        boolean allGranted = true;
        if (permissions != null && permissions.length != 0) {
            for(int i = 0; i < permissions.length; ++i) {
                allGranted = allGranted && isGranted(permissions[i]);
            }
            return allGranted;
        } else {
            return false;
        }
    }

    protected void openAppSettings() {
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        i.setData(uri);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(i);
    }

    public void showPermissionsDialog(String mensaje, DialogInterface.OnClickListener action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(mensaje);
        builder.setPositiveButton(R.string.common_accept, action);
        builder.setTitle(getString(R.string.permissions_title));
        builder.setNegativeButton(R.string.common_cancel, null);
        builder.show();
    }

    public String getPermissionsMessage(int requestCode) {
        String message = "";
        if (MY_PERMISSIONS_REQUEST_CAMERA == requestCode) {
            message = getString(R.string.permissions_camera);
        }
        return message;
    }


    /**
     * Handles the permissions success event
     *
     * @param requestCode
     */
    public abstract void onPermissionsSuccess(int requestCode);

    /**
     * Handles the denied permissions
     *
     * @param requestCode we use this code to know which permissions request was launched
     * @param permissions list that contains the permissions requested
     */
    public void onPermissionsDenied(final int requestCode, String[] permissions) {
        if (permissions != null && permissions.length > 0) {
            boolean shouldRequest = true;
            for (String s : permissions) {
                shouldRequest = shouldRequest && ActivityCompat.shouldShowRequestPermissionRationale(this, s);
            }

            final boolean shouldAskPermissionsAgain = shouldRequest;
            final DialogInterface.OnClickListener action = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (shouldAskPermissionsAgain)
                        checkPermissions(requestCode);
                    else {
                        openAppSettings();
                    }
                    dialog.dismiss();
                }
            };
            showPermissionsDialog(getPermissionsMessage(requestCode), action);

        }
    }

    private void checkPermissions(int requestCode) {
        if (MY_PERMISSIONS_REQUEST_CAMERA == requestCode)
            checkCameraPermissions();
        if (MY_PERMISSIONS_REQUEST_EXTERNAL == requestCode)
            checkExternalPermissions();
    }
}
