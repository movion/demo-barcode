# Barcode Instructions

**Library**

You can find the required libraries in the app/libs folder. Copy them in the same directory on your own app, then run the AS "Sync project with Gradle Files" command.

**Working with the reader**

Our hardware requires the Camera permission, so remember to ask for it. The app won't crash if you try to run the reader without the permission, but you won't be able to get any result.

The reader works in a secondary thread. If you need to update the UI with the scanned result you'll need to bring it to the main thread. The example we provided shows how to manage this issue.

Only one application can access the reader at a time, so remember to release it when your application is closed.
